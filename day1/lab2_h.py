def un_flatten(paths):
    dict1 = {}

    for path in paths:
        keys = path.split('/')
        temp_dict = dict1
        for sub_path in keys:
            if sub_path not in temp_dict:
                temp_dict[sub_path] = {}
            temp_dict = temp_dict[sub_path]

    return dict1


def test_un_flatten():
    tree = un_flatten(['A/B/T', 'A/U', 'A/U/Z'])
    assert tree == {
        'A': {
            'B': {'T': {}},
            'U': {
                'Z': {}
            }
        }
    }
    assert tree['A']['B']['T'] == {}
