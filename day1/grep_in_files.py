# Find all files in a directory recursively.
# Select only those whose names match a regex. Of those return only those who contain a line matching a regex.
# Print the full path of the matching file. Then print the matching line and the previous and next lines.
import os
import re
import pprint

def get_all_files(path):
    """
    function for retrieving all files in a given directory recursively
    """
    all_files = []

    for path, dirs, files in os.walk(path):
        local_files = [ '/'.join([path, file]) for file in files ]
        all_files += local_files

    return all_files


def filter_filenames(files, regexp):
    """
    function for filtering a list of filenames by a regex expression on the filenames
    """
    matching_files = [ file for file in files if re.search(regexp, os.path.basename(file), flags=re.I) ]

    return matching_files

def filter_content_files(files, regexp):
    """
    function for filtering a list of filenames by a regex expression on the content of the files
    """
    dt = {}

    for file in files:
        fh = open(file, 'r')
        line = fh.readline()
        prev_line = None
        while line:
            line = fh.readline()
            if re.search(regexp, line):
                dt[file] = (prev_line, line, fh.readline())
                break
            prev_line = line
        fh.close()

    return dt

try:
    files = get_all_files(os.getcwd())
    matching_files = filter_filenames(files, 'patterns')
    res = filter_content_files(matching_files, 'def ')
except Exception as e:
    print 'Exception raised\n\ttype: %s\n\tdetails: %s' % (e.__class__, e.args)
else:
    pprint.pprint(res, indent=4, depth=4, width=2)