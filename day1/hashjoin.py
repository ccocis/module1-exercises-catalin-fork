# Name      Dept ID       Dept     Dept ID
# -----------------       ----------------
# Smith       34          Sales        31
# Jones       33          Engineering  33
# Robinson    34          Clerical     34
# Jasper      36          Marketing    35
# Steinberg   33
# Rafferty    31
peeps = '''
Smith       34
Jones       33
Robinson    34
Jasper      36
Steinberg   33
Rafferty    31
'''
deps='''
Sales        31
Engineering  33
Clerical     34
Marketing    35
'''

def parse_tbl(t):
    pass

def create_index(column, tbl):
    pass

def hash_join(peeps, deps, peeps_depid_idx, deps_depid_idx):
    ret = []
    return ret

def test_parse_tbl():
    assert parse_tbl(deps) == [
        ('Sales', '31'),
        ('Engineering', '33'),
        ('Clerical', '34'),
        ('Marketing', '35'),
    ]

def test_create_index():
    d = parse_tbl(deps)
    assert create_index(1, d) == {'31': 0, '33': 1, '34': 2, '35': 3}

def test_hash_join():
    p = parse_tbl(peeps)
    peeps_depid_idx = create_index(1, p)
    d = parse_tbl(deps)
    deps_depid_idx = create_index(1, d)

    assert hash_join(p, d, peeps_depid_idx, deps_depid_idx) == [
        ('Steinberg', '33', 'Engineering', '33'),
        ('Rafferty', '31', 'Sales', '31'),
        ('Robinson', '34', 'Clerical', '34')
    ]