# -*- coding: utf-8 -*-

import urllib2
from codecs import open

url = u'http://ko.wikipedia.org/wiki/위키백과:대문'

url_handler = urllib2.urlopen(url.encode('utf-8'))

line = url_handler.readline()
idx = 0

fh = open('encoded_url', 'w', encoding='utf-8')

while line:
    idx += 1
    fh.write(str(idx) + ' ' + line.decode('utf-8'))
    line = url_handler.readline()

fh.close()